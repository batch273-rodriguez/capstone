// alert("capstone!");


class Customer {
    
    constructor(email) {
        this.email = email;
        this.cart = new Cart(); //instance of Cart class
        this.orders = [];

    }

    checkOut() { //if cart not empty push to orders array
        console.log(this.cart.contents.length);
        if(this.cart.contents.length>0){
            this.orders.push({products:this.cart.contents,totalAmount:this.cart.computeTotal().totalAmount});
            
        }
        return this;
    }

}

class Cart {

    constructor () {
        this.contents = [];
        this.totalAmount = 0;
    }

    addToCart(product,quantity) { // accepts a Product instance
        this.contents.push({product:product,quantity:quantity});
        return this;
    }

    showCartContents() {
        // console.log(this.contents);
        return this;
    }

    updateProductQuantity(product,quantity) { // (string,number)
        let foundIndex = this.contents.findIndex((item)=>{
            // console.log(item);
            // console.log(item.product.name);
            // console.log(product);
            // console.log(item.product.name===product);
            return item.product.name===product;
        })
        // console.log(foundIndex);
        // this.contents.product[foundIndex].quantity = quantity;
        this.contents[foundIndex].quantity = quantity
        return this;
    }

    clearCartContents() {
        this.contents = [];
        return this;
    }

    computeTotal() {
        let totalAmount = 0;
        this.contents.forEach(item => totalAmount = totalAmount + (item.product.price * item.quantity));
        this.totalAmount = totalAmount;
        return this;
    }

}

class Product {

    constructor (name,price) {
        this.name = name;
        this.price = price;
        this.isActive = true;

    }

    archive() {
        if (this.isActive) {this.isActive = false};
        return this;
    }

    updatePrice(price) {
        this.price = price;
        return this;
    }

}


// ------ unit testing

const john = new Customer('john@gmail.com');

const prodA = new Product('soap',9.99);
// const prodB = new Product('shampoo',4.99);

// john.cart.addToCart(prodA,3);
// john.cart.addToCart(prodB,5);

// console.log(john);
// console.log(prodA);
